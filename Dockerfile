FROM node:12 as build
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .

FROM node:12-slim
WORKDIR /app 
#COPY --from=build /app .
COPY . .
RUN npm install --production
EXPOSE 8080
CMD npm start